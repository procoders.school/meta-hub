# Meta Hub

Here you can find info on creating and structuring new Hubs.

### Foreword

Hubs are separate repositories that encompass the technologies the company uses. Separate hubs are mostly needed for different programming languages (like JavaScript, Ruby, PHP etc).

Hubs contain sections that represent bodies of knowledge of separate technologies within the hub. For example, The JS-Hub contains JS Fundamentals, React, Node, Vue, Angular, Architecture sections. A Ruby-Hub could, for instance, contain Ruby Fundamentals, Rails, Sinatra etc sections.

Each section has at least 2 sub-sections: learning materials and evaluation framework.

Learning materials are lists of books, articles, talks etc that are considered to be exceptionally helpful for building strong foundation in the corresponding technology. The lists must not be too big and should contain only the essential knowledge having acquired which the student will be able to continue to teach themselves effectively and will be able to distinguish good learning materials from bad and harmful.

Evaluation framework is normally the list of qualifications which has to be met by the student in order for the company to understand that a student has mastered the needed tech stack.

### Hub folder structure

[Repository Root]

    Section1_folder/
    
        - materials.md

        - evaluation.md

        - README.md

    Section2_folder/
    
        - materials.md

        - evaluation.md

        - README.md

    - README.md

[JS Hub](https://gitlab.com/procoders.school/js-hub) is a good starting example.
